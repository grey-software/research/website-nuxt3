export default defineNuxtConfig({
  app: {
    head: {
      title: "Omniworks",
      link: [
        {
          rel: "icon",
          type: "image/png",
          href: "favicon.ico",
          media: "(prefers-color-scheme: light)",
        },
        {
          rel: "icon",
          type: "image/png",
          href: "favicon-dark.ico",
          media: "(prefers-color-scheme: dark)",
        },
      ],
      meta: [
        { charset: "utf-8" },
        { name: "viewport", content: "width=device-width, initial-scale=1" },
        {
          name: "og:title",
          content: "Omniworks",
        },

        {
          name: "og:description",
          content:
            "Transform Your Brand With Skilled Professionals That You Can Trust!",
        },

        {
          name: "og:image",
          content: "/preview.png",
        },
      ],
      script: [],
    },
  },
  css: [
    "~/assets/main.css",
    "~/assets/typography.css",
  ],
  modules: [
    "nuxt-font-loader",
    "nuxt-icon",
    "@vueuse/nuxt",
    "@nuxtjs/tailwindcss",
  ],
  fontLoader: {
    local: [
      {
        src: "fonts/NeueHaasGroteskDisplay-Reg.otf",
        class: "nhgd-reg",
        family: "NeueHaasGroteskDisplayReg",
      },
      {
        src: "fonts/NeueHaasGroteskDisplay-Bold.otf",
        class: "nhgd-bold",
        family: "NeueHaasGroteskDisplayBold",
      },
    ],
  },
});
